package com.neilmonday.reader;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class FeedActivity extends Activity {

	private ListView listView;
	private ArrayList<String> feedUrls;
	private ArrayList<String> feedTitles;
	private ArrayList<MessageAdapter> messageAdapters;
	private Feed feed;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed);
        
        Bundle extras = getIntent().getExtras();
        feed = extras.getParcelable("feed");
        
        ListView listView = (ListView) findViewById(R.id.list_view);
        
        MessageAdapter adapter = new MessageAdapter(this, feed.getMessages());
        // Assign adapter to ListView
     	listView.setAdapter(adapter); 
    }
}

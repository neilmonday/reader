package com.neilmonday.reader;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import android.content.Context;
import android.util.Log;

public class SaxFeedParser extends BaseFeedParser {
	public List<Message> parse(String feed) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            RssHandler handler = new RssHandler();
            feed = feed.substring(4);
            Log.d("NEM", feed);
            InputStream stream = new ByteArrayInputStream(feed.getBytes("UTF-8"));
            parser.parse(stream, handler);
            return handler.getMessages();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } 
    }
}

package com.neilmonday.reader;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable, Comparable<Message>{
    static SimpleDateFormat FORMATTER = 
        new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
    private String title;
    private URL link;
    private String description;
    private Date date;
    
    public Message(){}

      // getters and setters omitted for brevity
    public void setLink(String link) {
        try {
            this.link = new URL(link);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public void setDate(String date) {
        // pad the date if necessary
        while (!date.endsWith("00")){
            date += "0";
        }
        try {
            this.date = FORMATTER.parse(date.trim());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
	public void setDescription(String description) {this.description = description;}
	public void setTitle(String title) {this.title = title;}

    public String getDate() {return FORMATTER.format(this.date);}
    public String getTitle() {return title;}
    public String getDescription() {return description;}
    public URL getLink() {return link;} //is this different from toString?

    @Override
    public String toString() {
    	return title + description;
    }

    @Override
    public int hashCode() {
    	return 0;//TODO compute hashCode
    }
    
    @Override
    public boolean equals(Object obj) {
    	return true; //TODO complete equals method
    }
      // sort by date
    public int compareTo(Message another) {
        if (another == null) return 1;
        // sort descending, most recent first
        return another.date.compareTo(date);
    }

    //PARCELABLE
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(title);
		out.writeSerializable(link);
		out.writeString(description);
		out.writeSerializable(date);
	}
	
    public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
		public Message createFromParcel(Parcel in) {
		    return new Message(in);
		}
		
		public Message[] newArray(int size) {
		    return new Message[size];
		}
	};
	
    private Message(Parcel in) {
		title = in.readString();
		link = (URL)in.readSerializable();
		description = in.readString();
		date = (Date)in.readSerializable();
    }
	//END PARCELABLE
}

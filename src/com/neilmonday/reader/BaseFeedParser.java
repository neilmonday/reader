package com.neilmonday.reader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;

public abstract class BaseFeedParser implements FeedParser {

    // names of the XML tags
    static final String PUB_DATE = "pubDate";
    static final  String DESCRIPTION = "description";
    static final  String LINK = "link";
    static final  String TITLE = "title";
    static final  String ITEM = "item";
    
    protected String feed = "";

    /*protected InputStream getInputStream(Context context) {
		try {
			InputStream inputStream = new ByteArrayInputStream(feed.getBytes("UTF-8"));
		    return inputStream;
		} catch (UnsupportedEncodingException e) {
		    e.printStackTrace();
		}
		
		return null;
    }*/
}
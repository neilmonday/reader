package com.neilmonday.reader;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Feed implements Parcelable{

	private String title;
	private List<Message> messages;
	private URL url;
	private int position;
	
	public Feed() {}
	
	public URL getUrl() {
		return url;
	}
	public void setUrl(URL url) {
		this.url = url;
	}
	public List<Message> getMessages() {
		return messages;
	}
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	
    //PARCELABLE
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(title);
		out.writeList(messages);
		out.writeSerializable(url); //this probably negates the benefits of parcelable
		out.writeInt(position);
	}
	
    public static final Parcelable.Creator<Feed> CREATOR = new Parcelable.Creator<Feed>() {
		public Feed createFromParcel(Parcel in) {
		    return new Feed(in);
		}
		
		public Feed[] newArray(int size) {
		    return new Feed[size];
		}
	};
	
    private Feed(Parcel in) {
		title = in.readString();
		messages = new ArrayList<Message>();
	    in.readList(messages, getClass().getClassLoader());
		url = (URL)in.readSerializable();
		position = in.readInt();
    }
	//END PARCELABLE
}

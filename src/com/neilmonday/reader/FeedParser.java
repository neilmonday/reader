package com.neilmonday.reader;

import java.util.List;

import android.content.Context;

public interface FeedParser {
    List<Message> parse(String feed);
}
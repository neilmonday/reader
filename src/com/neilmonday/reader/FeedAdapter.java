package com.neilmonday.reader;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FeedAdapter extends BaseAdapter {

	private List<Feed> feeds;
	private Context context;
	
	public List<Feed> getFeeds(){return feeds;}
	public void setFeeds(List<Feed> feeds ){this.feeds = feeds;}
	
	public FeedAdapter(Context context, List<Feed> feeds){
		this.context = context;
		this.feeds = feeds;
	}

	@Override
	public int getCount() {
		return feeds.size();
	}

	@Override
	public Object getItem(int arg0) {
		return feeds.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//return super.getView(position, convertView, parent);
		View row = convertView;
		if(row==null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
			row=inflater.inflate(R.layout.feed_item, parent, false);
		}
		
		TextView title=(TextView)row.findViewById(R.id.title);
		
		title.setText(feeds.get(position).getTitle());

		return row;
	}
}
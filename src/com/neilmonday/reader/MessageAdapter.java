package com.neilmonday.reader;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MessageAdapter extends BaseAdapter {

	private List<Message> messages;
	private Context context;
	
	public List<Message> getMessages(){return messages;}
	public void setMessages(List<Message> messages ){this.messages = messages;}
	
	public MessageAdapter(Context context, List<Message> messages){
		this.context = context;
		this.messages = messages;
	}

	@Override
	public int getCount() {
		return messages.size();
	}

	@Override
	public Object getItem(int arg0) {
		return messages.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		//return super.getView(position, convertView, parent);
		View row = convertView;
		if(row==null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
			row=inflater.inflate(R.layout.message_item, parent, false);
		}
		TextView title=(TextView)row.findViewById(R.id.title);
		TextView description=(TextView)row.findViewById(R.id.description);
		TextView date=(TextView)row.findViewById(R.id.date);
		
		title.setText(Html.fromHtml(messages.get(position).getTitle().toString()));//(messages.get(position).getTitle());
		description.setText(Html.fromHtml(messages.get(position).getDescription().toString()));
		date.setText(messages.get(position).getDate());

		return row;
	}
}
package com.neilmonday.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity {
	
	FeedAdapter adapter;
	private ArrayList<Feed> feeds = new ArrayList<Feed>();
	private ListView listView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		listView = (ListView) findViewById(R.id.list_view);
        adapter = new FeedAdapter(this, feeds);
		
        InputStream inputStream;
		try {
			inputStream = this.getAssets().open("feeds.txt");

	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	        StringBuilder total = new StringBuilder();
	        String line;
	
			while ((line = bufferedReader.readLine()) != null) {
			    readFeeds(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        // Assign adapter to ListView
     	listView.setAdapter(adapter); 
     	
     	
     	listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(MainActivity.this, FeedActivity.class);
				Bundle bundle = new Bundle();
				bundle.putParcelable("feed", feeds.get(position));
				intent.putExtras(bundle);
				startActivity(intent);
			}
     	});
	}
	 
    public void readFeeds(String url){
		class RetrieveFeedTask extends AsyncTask<String, Void, Feed> {
			private String result;
			
			@Override
			protected Feed doInBackground(String... urls) {
				Feed feed = new Feed();
				
				for (String urlString : urls) {
					try {
						URL url = new URL(urlString);
						URLConnection connection = url.openConnection();
						
						feed.setUrl(url);
				        feed.setTitle(url.toExternalForm());
						
						BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
						String s = "";
						while ((s = buffer.readLine()) != null) {
							result += s;
			  			}
			
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				SaxFeedParser parser = new SaxFeedParser();
		        ArrayList<Message>messages = (ArrayList<Message>) parser.parse(result);
		        feed.setMessages(messages);
				
				return feed;
			}
		
			@Override
			protected void onPostExecute(Feed feed) {
				feed.getMessages().remove(0); //I read the feed's info as if it were a post, which is not correct.
				feeds.add(feed);
				doneReadingFeed();
			}
		}
		
		RetrieveFeedTask task = new RetrieveFeedTask();
		task.execute( url );
    }
    
	//Enable the list item, and set the title?
    public void doneReadingFeed()
    {
    	MainActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapter.notifyDataSetChanged();
		    }
		});
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.add_feed:
                addFeed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    public void addFeed()
    {
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);

    	alert.setTitle("Add Feed");
    	alert.setMessage("RSS Feed URL:");

    	// Set an EditText view to get user input 
    	final EditText input = new EditText(this);
    	input.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
    	alert.setView(input);

    	alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				readFeeds(value);
			}
		});

    	alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
    	});

    	alert.show();
    }
}

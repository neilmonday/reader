Reader
======

An RSS reader for Android.

Build
=====

Reader is an Android app that has been built using Android 4.1, but should work with some older versions as well (API 8 and above). In order to build Reader, you first need to setup the Android SDK with Eclipse and the Eclipse ADT plugin. Follow this guide for more information on setting up your computer:

http://developer.android.com/sdk/installing/index.html

After setup, open Eclipse and go to File -> Import and choose the directory that you have Reader stored. Once imported, you may modify the 'assets/feeds.txt' file to add new feeds (see section below titled 'Add RSS Feeds'). Three default RSS feeds are setup already, so it is not necessary to modify 'feeds.txt'. To build and run Reader, just click the green run arrow at the top of Eclipse.

Usage
=====

Reader will asynchronously fetch and parse RSS feeds from the feeds.txt file located in the assets directory. This prevents the GUI from locking up, and also allows the user to view RSS feeds that have loaded while others are still downloading.

The first page will show you a list of all feeds in feeds.txt. You may add more feeds by tapping the "Add Feed" menu option on the main page and add the URL to your RSS feed. On the feed list, if you tap one of the feeds, then it will take you to see the entries for that feed. Press the back button to go back to the feed list.

Add RSS Feeds
=============

In the feeds.txt file in the 'assets' directory, add as many RSS feed URLs as you want (each separated by a new line). Reader will open each one and display a list for you to choose which one to look at.

